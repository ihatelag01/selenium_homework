﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualBasic;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;


namespace SeleniumDemo
{
    public class PracticeForm
    {

        private IWebDriver driver;
        public PracticeForm(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.Id, Using = "firstName")]
        private IWebElement firstName;

        [FindsBy(How = How.Id, Using = "lastName")]
        private IWebElement lastName;

        [FindsBy(How = How.Id, Using = "userEmail")]
        private IWebElement userEmail;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"genterWrapper\"]/div[2]/div[1]")]
        private IWebElement maleRadio;

        [FindsBy(How = How.Id, Using = "userNumber")]
        private IWebElement userNumber;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"dateOfBirthInput\"]")]
        private IWebElement dateOfBirthInput;

        [FindsBy(How = How.XPath,
            Using = "//*[@id=\"dateOfBirth\"]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/select")]
        private IWebElement month;

        [FindsBy(How = How.XPath,
            Using = "//*[@id=\"dateOfBirth\"]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div[2]/select")]
        private IWebElement year;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"dateOfBirth\"]/div[2]/div[2]/div/div/div[2]/div[2]/div[3]/div[7]")]
        private IWebElement day;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"subjectsContainer\"]/div/div[1]")]
        private IWebElement subjects;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"subjectsInput\"]")]
        private IWebElement subjectsInput;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"subjectsContainer\"]/div/div[1]/div[1]/div[1]")]
        private IWebElement subjectAutocompleteBox;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"hobbiesWrapper\"]/div[2]/div[1]")]
        private IWebElement sportsCheckbox;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"hobbiesWrapper\"]/div[2]/div[2]")]
        private IWebElement readingCheckbox;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"hobbiesWrapper\"]/div[2]/div[3]")]
        private IWebElement musicCheckbox;

        [FindsBy(How = How.Id, Using = "currentAddress")]
        private IWebElement currentAdress;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"state\"]/div/div[1]/div[1]")]
        private IWebElement state;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"city\"]/div/div[1]/div[1]")]
        private IWebElement city;
        public void StarUp()
        {
            driver.Url = "https://demoqa.com/automation-practice-form";
            driver.Manage().Window.Maximize();
        }

        public void EnterName(string string1, string string2)
        {
            firstName.SendKeys(string1);
            lastName.SendKeys(string2);
        }

        public void EnterEmail(string email)
        {
            userEmail.SendKeys(email);
        }

        public void CheckGender()
        {

            maleRadio.Click();
        }

        public void EnterNumber(string number)
        {
            userNumber.SendKeys(number);
        }

        public void SelectBirthday()
        {
            dateOfBirthInput.Click();
            SelectElement selectedMonth = new SelectElement(month);
            selectedMonth.SelectByValue("4");
            SelectElement selectedYear = new SelectElement(year);
            selectedYear.SelectByValue("1999");
            day.Click();
        }

        public void EnterSubjects(string str)
        {
            subjects.Click();
            Thread.Sleep(1000);
            subjectsInput.SendKeys(str);
            Thread.Sleep(1000);
            Actions action = new Actions(this.driver);
            action.SendKeys(Keys.Enter).Perform();
            Thread.Sleep(2000);
        }

        public void CheckHobbies()
        {
            sportsCheckbox.Click();
            readingCheckbox.Click();
            musicCheckbox.Click();
        }

        public void EnterAdress(string adress)
        {
            currentAdress.SendKeys(adress);
        }

        public void Quit()
        {
            Thread.Sleep(5000);
            driver.Quit();
        }

        public void SelectStateCity()
        {
            state.Click();
            Actions action = new Actions(this.driver);
            action.SendKeys(Keys.Enter).Perform();
            Thread.Sleep(1000);
            city.Click();
            action.SendKeys(Keys.Enter).Perform();
        }



    }
}
