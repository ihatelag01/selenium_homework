﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumExtras.PageObjects;

namespace SeleniumDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            ChromeDriver driver = new ChromeDriver();
            PracticeForm form = new PracticeForm(driver);
            form.StarUp();
            form.EnterName("Alexandru","Vizitiu" );
            form.EnterEmail("user@domain.com");
            form.CheckGender();
            form.EnterNumber("1111111111"); 
            form.SelectBirthday();
            form.EnterSubjects("com");
            form.CheckHobbies();
            form.EnterAdress("1 main road");
            form.SelectStateCity();
            form.Quit();
        }
    }
}
